package com.example.pocsignature

import android.content.Context
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.caverock.androidsvg.SVGParseException
import com.example.pocsignature.ui.theme.PocSignatureTheme
import se.warting.signaturepad.SignaturePadAdapter
import se.warting.signaturepad.SignaturePadView
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val signatureViewModel: SignatureViewModel by viewModels()

        setContent {
            PocSignatureTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = "home") {
                        composable("home") {
                            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                            HomeScreen(navController)
                        }
                        composable("signature") {
                            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                            SignatureScreen(navController, viewModel = signatureViewModel)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun HomeScreen(navController: NavHostController) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Button(
            onClick = {
                navController.navigate("signature")
            },
            modifier = Modifier.padding(16.dp)
        ) {
            Text("Open Signature Screen")
        }
    }
}

@Composable
fun SignatureScreen(navController: NavHostController, viewModel: SignatureViewModel) {
    val context = LocalContext.current
    var signaturePadAdapter: SignaturePadAdapter? by remember { mutableStateOf(null) }
    val snackbarHostState = remember { SnackbarHostState() }
    val coroutineScope = rememberCoroutineScope()

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        // Right section takes 2/3 of the space
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(2f)
                .background(Color.White),
        ) {

            if (viewModel.showSuccessSnackbar) {
                Snackbar(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.TopCenter), // Position the Snackbar at the top center
                    action = {
                        TextButton(
                            onClick = {
                                viewModel.showSuccessSnackbar = false
                                navController.navigate("home") // Navigate back to "home"
                            }
                        ) {
                            Text("Voltar")
                        }
                    }
                ) {
                    Text("Signature saved successfully!")
                }
            }
            // Signature Pad
            SignaturePadView(
                onReady = {
                    signaturePadAdapter = it
                }
            )


        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f) // Left section takes 1/3 of the space
                .background(Color.Gray),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            // Clear button
            Button(
                onClick = {
                    signaturePadAdapter?.clear()
                },
                modifier = Modifier.padding(16.dp)
            ) {
                Text("Clear")
            }

            // Save button
            Button(
                onClick = {
                    saveSignature(adapter = signaturePadAdapter, context = context)
                    viewModel.showSuccessSnackbar = true

                },
                modifier = Modifier.padding(16.dp)
            ) {
                Text("Save")
            }
        }
    }
}


private fun saveSignature(context: Context,adapter: SignaturePadAdapter?) {
    val signatureSvg = adapter?.getSignatureBitmap()
    if (signatureSvg != null ) {
        try {
            // Create a directory in the Downloads folder
            val downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val fileDir = File(downloadsDir, "SignatureApp")
            if (!fileDir.exists()) {
                fileDir.mkdirs()
            }

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
            val fileName = "signature_$timeStamp.png"
            val file = File(fileDir, fileName)


            val fos = FileOutputStream(file)
            signatureSvg.compress(Bitmap.CompressFormat.PNG, 100, fos)
            fos.close()


            // Provide feedback to the user, e.g., display a Toast.
        } catch (e: SVGParseException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    } else {
        Toast.makeText(context, "No signature to save", Toast.LENGTH_SHORT).show()
    }
}




@Preview
@Composable
fun MyComposablePreview() {
//    SignatureScreen()
}

