package com.example.pocsignature

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class SignatureViewModel : ViewModel() {
    var showSuccessSnackbar by mutableStateOf(false)
}